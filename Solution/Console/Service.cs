﻿using Quartz;
using Quartz.Impl;
using Serilog;

namespace Console
{
    public class Service
    {
        private readonly IScheduler _scheduler;
        private readonly ILogger _logger;
        private readonly Configuration _config;

        public Service(ILogger logger)
        {
            _scheduler = StdSchedulerFactory.GetDefaultScheduler();
            _config = Configuration.Instance;
            _logger = logger;
        }

        public void Start()
        {

            try
            {
                _scheduler.Start();
                ScheduleJobs();
            }
            catch (System.Exception ex)
            {
                _logger.Error(ex, ex.Message);
            }
        }

        private void ScheduleJobs()
        {
            IJobDetail job = JobBuilder.Create<LogicJob>()
           .WithIdentity("job1", "group1")
           .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(_config.LoopInMinutes)
                    .RepeatForever())
                .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void Stop()
        {
            try
            {
                _scheduler.Shutdown();
            }
            catch (System.Exception ex)
            {
                _logger.Error(ex, ex.Message);
            }
        }
    }
}
