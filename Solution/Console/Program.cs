﻿using Serilog;
using System;
using System.IO;
using Topshelf;

namespace Console
{
    public static class Program
    {
        static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>
            {

                Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File($"{Directory.GetCurrentDirectory()}\\logs\\log.txt", rollingInterval: RollingInterval.Day)
                    .CreateLogger();
                x.UseSerilog(Log.Logger);

                x.Service<Service>(s =>
                {
                    s.ConstructUsing(_ => new Service(Log.Logger));
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Topshelf with Quartz and Serilog");
                x.SetDisplayName("Topshelf with Quartz");
                x.SetServiceName("Topshelf-Quartz");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
