﻿using System.Configuration;

namespace Console
{
    public class Configuration
    {
        private static Configuration _instance;
        public static Configuration Instance => _instance ?? (_instance = new Configuration());

        public Configuration()
        {
            GetSettings();
        }

        private void GetSettings()
        {
            ConfigurationManager.RefreshSection("appSettings");
            int.TryParse(ConfigurationManager.AppSettings["LoopInMinutes"], out int loopInMinutes);
            LoopInMinutes = loopInMinutes;
        }

        public int LoopInMinutes { get; set; }
    }
}
