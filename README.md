# base-windows-service-using-topshelf-and-quartz
This is supposed to be a bootstraping repo for eliminating boilerplate code
when creating windows services. With Topshelf you can install a console app
as a windows service with Example.exe install and it will take care of 
everything for you and with quartz, you can schedule an action to run at regular
intervals. In enterprise, this base code is used quite a lot.

